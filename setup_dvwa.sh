#!/bin/bash

sed -i 's/allow_url_include = Off/allow_url_include = On/g' /etc/php5/apache2/php.ini && \
sed -i 's/FileInfo/All/g' /etc/apache2/sites-available/000-default.conf && \
echo 'session.save_path = "/tmp"' >> /etc/php5/apache2/php.ini
